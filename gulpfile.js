'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    concat = require("gulp-concat"),
    order = require("gulp-order"),
    babel = require("gulp-babel");

var path = {
    public: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'public/',
        js: 'public/js/',
        css: 'public/css/',
        images: 'public/images/',
        fonts: 'public/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/**/*.js',//В стилях и скриптах нам понадобятся только main файлы
        css: 'src/**/*.scss',
        images: 'src/images/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        css: 'src/css/**/*.scss',
        images: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './public',
    nodes: './node_modules'
};

var config = {
    server: {
        baseDir: "./public"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "gulp_test"
};

gulp.task('clean', function (cb)
{
    rimraf(path.clean, cb);
});

gulp.task('webserver', function ()
{
    browserSync(config);
});

gulp.task('html:build', function ()
{
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(gulp.dest(path.public.html)) //Выплюнем их в папку public
    .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function ()
{
    gulp.src([path.nodes + '/jquery/dist/jquery.js', path.nodes + '/angular/angular.js', path.src.js]) //Найдем наш main файл
        .pipe(sourceMaps.init()) //Инициализируем sourcemap
        .pipe(order(['jquery.js', 'angular.js', 'js/main.js']))
        .pipe(babel())
        .pipe(uglify()) //Сожмем наш js
        .pipe(concat('script.js'))
        .pipe(sourceMaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.public.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('css:build', function ()
{
    gulp.src([path.nodes + '/normalize-css/normalize.css', path.src.css]) //Выберем наш main.scss
        .pipe(sourceMaps.init()) //То же самое что и с js
        .pipe(order(['normalize.css','css/main.scss']))
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(concat('style.css'))
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(path.public.css)) //И в build
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function ()
{
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.public.fonts))
});

gulp.task('image:build', function ()
{
    gulp.src(path.src.images) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.public.images)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'css:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function ()
{
    watch([path.watch.html], function (event, cb)
    {
        gulp.start('html:build');
    });
    watch([path.watch.css], function (event, cb)
    {
        gulp.start('css:build');
    });
    watch([path.watch.js], function (event, cb)
    {
        gulp.start('js:build');
    });
    watch([path.watch.images], function (event, cb)
    {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function (event, cb)
    {
        gulp.start('fonts:build');
    });
});

gulp.task('default', ['build', 'webserver', 'watch']);